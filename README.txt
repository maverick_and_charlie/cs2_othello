CS2 Assignment 10 - Kate Lewis and Rushikesh Joshi
Kate: Implemented the AI interface by creating a player that made moves randomly. We added functions to calculate the possible moves each round and to calculate the score.  Then upgraded the AI by doing the heuristic function as well.
Rush: Implmented the minimax and looked up the game online to upgrade the scoring function by changing the weights of moves.  Also, helped debug the code.

Most of the work was done together except for the random player.

To prepare for the tournament, we updated the heuristic function.  We attempted to do alpha beta but it didn't work so we resorted back to our original minimax.  