#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <map>
#include <climits>
using namespace std;

class Player {
	Board board;
	Side side;
	Side opp;
    int *mult[8][8];
public:
    Player(Side si);
    ~Player();
	int getScore(Move *move, Side side, Board boar);

    
    Move *doMove(Move *opponentsMove, int msLeft);

    Move *minimax(Board updated_board, int times);
    Move *alphabeta(Board updated_board, int times, int alpha, int beta);


    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

};

#endif
