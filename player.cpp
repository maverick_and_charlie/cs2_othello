#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side si) {
    // Will be set to true in test_minimax.cpp.

    testingMinimax = true;
    side = si;
    opp = (side == BLACK) ? WHITE : BLACK;
        if(side == BLACK)
    {
            fprintf(stderr, "I am black\n");

    }
    else
    {
        fprintf(stderr, "I am white\n");

    }





    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */


}

// Here is the first change for extra credit!!
// hi
/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
 int Player::getScore(Move *move, Side sid, Board boar)
 {
//    int multi[8][8] = {{99, -8, 8, 6, 6, 8, -8, 99}, {-8, -24, -4, -3, -3, -4, -24, -8}, {8, -4, 7, 4, 4, 7, -4, 8}, {6, -3, 4, 0, 0, 4, -3, 6}, {6, -3, 4, 0, 0, 4, -3, 6}, {8, -4, 7, 4, 4, 7, -4, 8}, {-8, -24, -4, -3, -3, -4, -24, -8}, {99, -8, 8, 6, 6, 8, -8, 99}};

    Board tem = *boar.copy();
    Board temp = tem;
    temp.doMove(move, sid);

    int x = move->getX();
    int y = move->getY();
    
    int mult = 1;

    if((x == 0 && y == 0) || (x == 7 && y == 7) || (x == 0 && y == 7) || (x == 7 && y == 0))
    {
        mult = 99;
    }
    else if ((x == 0 && (y != 6 || y != 1)) || (x == 7 && (y != 6 || y != 1)) || (y == 7 && (x != 6 || x != 1)) || (y == 0 && (x != 6 || x != 1)))
    {
        mult = 3;
    }
    else if ((x == 1 && y == 1) || (x == 6 && y == 6) || (x == 6 && y == 0) || (x = 0 && y == 6) || (x == 0 && (y == 6 || y == 1)) || (x == 7 && (y == 6 || y == 1)) || (y == 7 && (x == 6 || x == 1)) || (y == 0 && (x == 6 || x == 1)))
    {
        mult = -3;

    }

    

    if(sid == BLACK)
    {
        return mult * (temp.countBlack() - temp.countWhite()) - temp.getMoves(WHITE).size() * 10;
    }
    else
    {
        return mult * (temp.countWhite() - temp.countBlack())- temp.getMoves(BLACK).size() * 10;
    }
    return 0;
 }


Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     /*
     char boardData[64] = {
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', 'b', ' ', ' ', ' ', ' ', ' ', ' ', 
        'b', 'w', 'b', 'b', 'b', 'b', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '
    };
     
    board.setBoard(boardData);
    return minimax(board);
*/


    int times = 0;

     board.doMove(opponentsMove, opp);
     Move* ourmove = minimax(board, times);
     board.doMove(ourmove, side);
     return ourmove;



     /*

     vector<Move*> moves = board.getMoves(side);
     map<Move*, int> move_list;
     Move *move;

     if(!moves.empty())
     {
        
        move = moves.back();
        moves.pop_back();
        
        move_list[move] = getScore(move, side, board);

     
        while(!moves.empty())
         {

            Move *move2 = moves.back();
            moves.pop_back();
            move_list[move2] = getScore(move2, side, board);
            if(move_list[move2] > move_list[move])
            {
                move = move2;
            }

            //set  move to max score

         }
     }
     else
     {
        return NULL;  
     }
     board.doMove(move, side);    
     return move;

     */
    
}

Move *Player::minimax(Board updated_board, int times)
{
    if(updated_board.hasMoves(side))
    {

        vector<Move*> moves = updated_board.getMoves(side); // it is our turn to play

        map<Move*, int> move_scores;
        Move *move;

        Move *move2;
        while(!moves.empty())
        {
            move = moves.back();
            moves.pop_back();
            Board tem = *updated_board.copy();
            Board temp = tem;
            temp.doMove(move, side);
            vector<Move*> tempMoves = temp.getMoves(opp);

        
            
            int max = INT_MIN;
            int score = 0;

            while(!tempMoves.empty())
            {
                move2 = tempMoves.back();
                tempMoves.pop_back();
                Board temp2 = temp;
                temp2.doMove(move2, opp);
                if(times < 4 && minimax(temp2, times + 1) != NULL)
                {
                    
                    score = minimax(temp2, ++times)->getScore();
                }
                else {

                score = getScore(move2, opp, temp2);
                }

                if(score > max)
                {
                    max = score;
                }
            }
            move_scores[move] = max;
            
        }
        

        /*moves = updated_board.getMoves(side);
        move = moves.back();
        moves.pop_back();*/

        //fprintf(stderr, "(%d, %d) %d\n", move->getX(), move->getY(), move_scores[move]);
        int minn = INT_MAX;
        map<Move*, int>::iterator i;
        for(i = move_scores.begin(); i != move_scores.end(); i++)
        {
            if(i->second <  minn)
            {
                minn = i->second;
                move = i->first;
            }
        }
        


     
        /*while(!moves.empty())
         {

            move2 = moves.back();
            moves.pop_back();
            fprintf(stderr, "(%d, %d) %d\n", move2->getX(), move2->getY(), move_scores[move2]);
            if(move_scores[move2] < move_scores[move])
            {
                move = move2;
            }

            //set  move to max score

         }
        */
         move->score = minn;
         return move;
    }
    return NULL;
}


Move *Player::alphabeta(Board updated_board, int times, int alpha, int beta)
{
    if(updated_board.hasMoves(side))
    {

        vector<Move*> moves = updated_board.getMoves(side); // it is our turn to play

        map<Move*, int> move_scores;
        Move *move;

        Move *move2;
        while(!moves.empty())
        {
            move = moves.back();
            moves.pop_back();
            Board tem = *updated_board.copy();
            Board temp = tem;
            temp.doMove(move, side);
            vector<Move*> tempMoves = temp.getMoves(opp);

        
            
            int max = INT_MIN;
            int score = 0;

            while(!tempMoves.empty())
            {
                move2 = tempMoves.back();
                tempMoves.pop_back();
                Board temp2 = temp;
                temp2.doMove(move2, opp);
                if(times < 3 && minimax(temp2, times + 1) != NULL)
                {
                    
                    score = minimax(temp2, ++times)->getScore();
                }
                else {

                score = getScore(move2, opp, temp2);
                }

                if(score > max)
                {
                    max = score;
                }
            }
            move_scores[move] = max;
            
        }
        

        /*moves = updated_board.getMoves(side);
        move = moves.back();
        moves.pop_back();*/

        //fprintf(stderr, "(%d, %d) %d\n", move->getX(), move->getY(), move_scores[move]);
        int minn = INT_MAX;
        map<Move*, int>::iterator i;
        for(i = move_scores.begin(); i != move_scores.end(); i++)
        {
            if(i->second <  minn)
            {
                minn = i->second;
                move = i->first;
            }
        }
        


     
        /*while(!moves.empty())
         {

            move2 = moves.back();
            moves.pop_back();
            fprintf(stderr, "(%d, %d) %d\n", move2->getX(), move2->getY(), move_scores[move2]);
            if(move_scores[move2] < move_scores[move])
            {
                move = move2;
            }

            //set  move to max score

         }
        */
         move->score = minn;
         return move;
    }
    return NULL;
}